import Container from "@/components/container";
import { urlForImage } from "@/lib/sanity/image";
import Image from "next/image";
import Link from "next/link";

export default function About({ authors, settings }) {
    return (
        <Container>
            <div className="flex items-center justify-center">
                <h1 className="text-brand-primary mb-3 mt-2 text-center text-3xl mt-20 font-semibold tracking-tight dark:text-white lg:text-4xl lg:leading-snug">
                    Rreth Eti-t
                </h1>
                <div className="flex items-center justify-center">
                    <img
                        src="https://upload.wikimedia.org/wikipedia/commons/c/c3/Eti_logo.png"
                        alt="Eti logo"
                        className="object-cover"
                    />
                </div>
            </div>

            {/*<div className="mb-16 mt-6 grid grid-cols-3 gap-5 md:mb-32 md:mt-16 md:gap-16">*/}
            {/*  {authors.slice(0, 3).map(author => {*/}
            {/*    const imageProps = urlForImage(author?.image) || null;*/}
            {/*    return (*/}
            {/*      <div*/}
            {/*        key={author._id}*/}
            {/*        className="relative aspect-square overflow-hidden rounded-md bg-slate-50 odd:translate-y-10 odd:md:translate-y-16">*/}
            {/*        <Link href={`/author/${author?.slug}`}>*/}
            {/*          {imageProps && (*/}
            {/*            <Image*/}
            {/*              src={imageProps?.src}*/}
            {/*              alt={author?.name || " "}*/}
            {/*              fill*/}
            {/*              sizes="(max-width: 320px) 100vw, 320px"*/}
            {/*              className="object-cover"*/}
            {/*            />*/}
            {/*          )}*/}
            {/*        </Link>*/}
            {/*      </div>*/}
            {/*    );*/}
            {/*  })}*/}
            {/*</div>*/}

            <div className="prose mx-auto mt-4 text-center dark:prose-invert">
                <p>
                    Objektivi ynë kryesor është të lejojmë individët t'i bashkohen kompanisë sonë si punonjës të lumtur që janë të hapur ndaj ndryshimit, zhvillimit dhe konkurrencës, si dhe energjikë, entuziastë, të etur për të përmirësuar punën dhe veten e tyre, shumë të motivuar, të aftë për t'iu përmbajtur kompetencave dhe vlerave të ETI dhe bartja e ETI në të ardhmen.
                </p>
                <p>
                    Për këtë qëllim, ne zbatojmë metodologji sipas standardeve ndërkombëtare, përshtatim aftësitë e duhura me pozicionet e duhura dhe marrim vendimin më të mirë për të çuar kompaninë dhe punonjësit tanë në të ardhmen.
                </p>
                <p>
                    <Link href="/contact">Kontaktoni</Link>
                </p>
            </div>
        </Container>
    );
}
